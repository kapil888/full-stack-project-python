from models import Profile,Academic,School,College
from rest_framework.views import APIView
from rest_framework.response import Response
from serializers import ProfileSerializer,AuthCustomSerializer,PkSerializer,SchoolSerializer,CollegeSerializer,EditProfileSerializer,AcademicSerializer,ImageSerializer
from rest_framework import status
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate
import json
import base64
from django.contrib.auth.models import User



def Success_response(obj):
    content = {
                    'status': {
                        'isSuccess': True,
                        'code': "SUCCESS",
                        'message': "Success",
                    },
                }

    for key, value in obj.iteritems():
         content['status'][key] = value
    return Response(content, status=status.HTTP_200_OK)


def fail_response(obj):
    content = {
                    'status': {
                        'isSuccess': False,
                        'code': "fail",
                        'message': "failed",
                    },
                }

    for key, value in obj.iteritems():
         content['status'][key] = value
    return Response(content, status=status.HTTP_200_OK)


# {"email":"kapilsh2014@gmail.com","password":"a"}

# {"email":"admin@gmail.com","password":"kapil123"}

class LoginFun(APIView):
	def post(self,request):
		try:
		 	serializer = AuthCustomSerializer(data=request.data)
		 	if serializer.is_valid():
		 		email = request.data['email']
		 		password = request.data['password']
		 		try:
		 			user_email_obj = User.objects.get(email__iexact=email)
		 		except Exception as e:
		 			return fail_response({'error':str(e)})
		 			user_email_obj = None

		 		


		 		if user_email_obj:
		 			user_obj = authenticate(username=user_email_obj.username,password=password)

		 			if user_obj:
		 				token,create = Token.objects.get_or_create(user=user_obj)
		 				profile_obj = Profile.objects.filter(user=user_obj).first()
		 				academica_obj = Academic.objects.filter(profile=profile_obj)


						ob = {
						'token':token.key,
						'profile':ProfileSerializer(profile_obj).data,
						'Academic':AcademicSerializer(academica_obj,many=True).data
						}
						return Success_response(ob)
					else:
						return fail_response({'error':'password is wrong'})

				else:
					return fail_response({'error':'user not found'})
			else:
				return fail_response({'error':serializer.errors})

		except Exception as e:
			return fail_response({'error':e})


class Registation(APIView):
	def post(self,request):
		try:
			serializer = PkSerializer(data=request.data)


			if serializer.is_valid():
				email = request.data['email']
				phone = request.data['phone']
				name = request.data['name']
				password = request.data['password']

				print password


		 		try:
		 			user_email_obj = User.objects.filter(username=email,email=email)
		 			print user_email_obj
		 			if user_email_obj:
		 				return fail_response({'error':'user alreday exist'})
		 			else:
		 				user_obj = User.objects.create(username=email,email=email)
		 				user_obj.set_password(password)
		 				user_obj.save()
		 				token = Token.objects.create(user=user_obj)
		 				profile_obj = Profile.objects.create(user=user_obj,name=name,email=email,phone=phone)
		 				academica_obj = Academic.objects.create(profile=profile_obj)

		 				objt = {
		 				'profile':ProfileSerializer(profile_obj).data,
		 				'Token':token.key,
		 				}

		 				return Success_response(objt)

		 		except Exception as e:
		 			return fail_response({'error':str(e)})
			
			else:
				return fail_response({'error':str(serializer.errors)})


		except Exception as e:
			return fail_response({'error':e})
		



class Profile_detail(APIView):
    def get(self, request):
    	try:
    		print request.auth
    		if request.auth:
    			pro = Profile.objects.filter(user=request.user).first()
		    	profile = ProfileSerializer(pro).data
		    	academica_obj = Academic.objects.filter(profile=pro).first()
		    	obj = {
		    	  'profile':profile,
		    	  'academica_obj':AcademicSerializer(academica_obj).data
		    	}
		        return Success_response(obj)
    	except Exception as e:
    		obj = {
    		 'error':e
    		}
    		return fail_response(obj) 



class EditProfile(APIView):
	def post(self,request):
		try:
			serializer = EditProfileSerializer(data=request.data)
			if serializer.is_valid():
				if request.auth:
					profile_obj = Profile.objects.filter(user=request.user).first()
					name = serializer.validated_data.get('name')
					phone = serializer.validated_data.get('phone')
					school_list = serializer.validated_data.get('school_list')
					gradution_list = serializer.validated_data.get('gradution_list')
					Post_gradution_list = serializer.validated_data.get('Post_gradution_list')
					other = serializer.validated_data.get('other')
					print other


					academica_obj = Academic.objects.filter(profile=profile_obj).first()
					if academica_obj:
						pass
					else:
						academica_obj = Academic.objects.create(profile=profile_obj)
					

					if other:
						academica_obj.other = other
						academica_obj.save()





					if school_list:
						try:
							print 'school'
							school_objs = School.objects.filter(profile=profile_obj)
							school_objs.delete()
						except:
							pass

					if gradution_list:
						try:
							college_obj = College.objects.filter(profile=profile_obj)
							college_obj.delete()
						except:
							pass	

					if Post_gradution_list:
						try:
							post_college_obj = College.objects.filter(profile=profile_obj)
							post_college_obj.delete()
						except:
							pass


					for i in json.loads(school_list):
						print i['school_name']
						school_obj_new = School.objects.create(profile=profile_obj,school_name=str(i['school_name']),board=str(i['school_Board']),student_class=str(i['school_class']),passing_year=str(i['school_Year']))
						academica_obj.school.add(school_obj_new)
						academica_obj.save()




					for i in json.loads(gradution_list):
					 	graduation_obj_new = College.objects.create(profile=profile_obj,college_name=i['graduation_collge_name'],degree_name=i['graduation_course_name'],passing_year=i['graduation_course_year'])
					 	academica_obj.graduation.add(graduation_obj_new)
					 	academica_obj.save()


					for i in json.loads(Post_gradution_list):
					 	post_obj_new = College.objects.create(profile=profile_obj,college_name=i['post_graduation_college_name'],degree_name=i['post_graduation_course_name'],passing_year=i['post_graduation_course_year'])
					 	academica_obj.post_graduation.add(post_obj_new)
					 	academica_obj.save()

					return Success_response({'academica_obj':AcademicSerializer(academica_obj).data})
				else:
					return fail_response({'error':'invalid user'})

			else:
				return fail_response({'error':serializer.errors})


		except Exception as e:
			return fail_response({'error':e}) 


class ImageUpload(APIView):

	def post(self,request): 
		serializer = ImageSerializer(data=request.data)
		profile_obj = Profile.objects.filter(user=request.user).first()

		profile_obj.image = request.data['image']
		print base64.b64decode(str(request.data['image']))
		profile_obj.save()

		if serializer.is_valid():
			# profile_obj.image = validated_data.get('image')
			# profile_obj.save()
			return Success_response({'academica_obj':'k'})
		else:
			return Success_response({'academica_obj':serializer.errors})
		
	
