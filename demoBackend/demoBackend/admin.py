from django.contrib import admin
from models import Profile,School,College,Academic 


admin.site.register(Profile)
admin.site.register(School)
admin.site.register(College)
admin.site.register(Academic)