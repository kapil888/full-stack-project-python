from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    phone = models.CharField(max_length=10,blank=True, null= True)
    email = models.EmailField(max_length=70,blank=True,null= True)
    image = models.ImageField(blank=True, null= True,upload_to='image/')


    def __unicode__(self):
    	return self.name 

class School(models.Model):
	profile = models.ForeignKey(Profile,on_delete=models.CASCADE)
	school_name = models.TextField(blank=True, null= True)
	board = models.TextField(blank=True, null= True)
	student_class = models.TextField(blank=True, null= True)
	passing_year = models.CharField(max_length=4,blank=True,null=True)

	def __unicode__(self):
		return self.school_name


class College(models.Model):
	profile = models.ForeignKey(Profile,on_delete=models.CASCADE)
	college_name = models.TextField(blank=True, null= True)
	degree_name = models.TextField(blank=True, null= True)
	passing_year = models.CharField(max_length=4,blank=True,null=True)


	def __unicode__(self):
		return self.college_name



class Academic(models.Model):
	profile = models.OneToOneField(Profile,on_delete=models.CASCADE)
	school = models.ManyToManyField(School,related_name='school')
	graduation = models.ManyToManyField(College,related_name='graduation')
	post_graduation = models.ManyToManyField(College,related_name='post_graduation')
	other = models.TextField(blank=True, null= True)

	def __unicode__(self):
		return "profile: {} || id {}".format(self.profile,self.id) 



		
