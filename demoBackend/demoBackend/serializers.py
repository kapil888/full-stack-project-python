from rest_framework import serializers
import models 
from drf_extra_fields.fields import Base64ImageField

class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Profile
        fields = '__all__'


class SchoolSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.School
        fields = '__all__'


class CollegeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.College
        fields = '__all__'



class ImageSerializer(serializers.Serializer):
    profile_image = Base64ImageField(
        max_length=None, use_url=True, allow_empty_file=False, required=False
    )
        



class AcademicSerializer(serializers.ModelSerializer):
	graduation = serializers.SerializerMethodField()
	post_graduation = serializers.SerializerMethodField()
	school = serializers.SerializerMethodField()


	class Meta:
		model = models.Academic
		fields = '__all__'



        def get_graduation(self, obj):
        	data = obj.graduation 
        	if data:
        		return CollegeSerializer(data,many=True).data
        	else:
        		return []

        def get_post_graduation(self, obj):
        	data = obj.post_graduation
        	if data:
        		return CollegeSerializer(data,many=True).data
        	else:
        		return []

        def get_school(self, obj):
        	data = obj.school
        	if data:
        		return SchoolSerializer(data,many=True).data
        	else:
        		[]



class AuthCustomSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()


class EditProfileSerializer(serializers.Serializer):
    other = serializers.CharField(required=False,allow_blank=True)
    name = serializers.CharField()
    phone = serializers.CharField()
    school_list = serializers.JSONField()
    gradution_list = serializers.JSONField()
    Post_gradution_list = serializers.JSONField()
    


class PkSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    name = serializers.CharField(required=True)
    phone = serializers.CharField(required=True)
    password = serializers.CharField(required=True)
 


