Django==1.11.25
django-cors-headers==3.0.2
django-extra-fields==1.2.4
djangorestframework==3.9.4
Pillow==6.2.0
pytz==2019.3
